var fs = require("fs");
var archiver = require("archiver");

let srcFolder = "src";
let zipFile = "extension.zip";

const webStore = require("chrome-webstore-upload")({
  extensionId: process.env.EXTENSION_ID,
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  refreshToken: process.env.REFRESH_TOKEN,
});

zipFolder(srcFolder, zipFile)
  .then((outFile) => webStore.uploadExisting(outFile))
  .then((res) => {
    webStore
      .publish()
      .then((res) => {
        console.log("Successfully published extension");
      })
      .catch((error) => {
        console.log(`Error while publishing publishing extension: ${error}, ${error.response.body}`);
        process.exit(1);
      });
  })
  .catch((error) => {
    console.log(`Error while uploading extension: ${error}, ${error.response.body}`);
    process.exit(1);
  });

function zipFolder(folder, zipName, options = {}) {
  return new Promise((resolve, reject) => {
    const output = fs.createWriteStream(zipName);
    const archive = archiver("zip");

    output.on("close", function () {
      let outputStream = fs.createReadStream(`./${zipFile}`);
      resolve(outputStream);
    });

    archive.on("error", function (err) {
      reject(err);
    });

    archive.pipe(output);

    options = Object.assign(
      {},
      {
        ignore: "**/*.map",
        cwd: folder,
        dot: false,
        stat: false,
      },
      options
    );

    archive.glob(`**`, options);

    archive.finalize();
  });
}
