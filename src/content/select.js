const selectId = "themeSelect";

function showSelect() {
  var select = document.getElementById(selectId);
  if (select) {
    return;
  }
  select = document.createElement("select");
  select.id = selectId;
  select.innerHTML = `
      <option selected="" value="default">default</option>
      <option value="ambiance">ambiance</option>
      <option value="base16-dark">base16-dark</option>
      <option value="base16-light">base16-light</option>
      <option value="blackboard">blackboard</option>
      <option value="cobalt">cobalt</option>
      <option value="darcula">darcula</option>
      <option value="dracula">dracula</option>
      <option value="eclipse">eclipse</option>
      <option value="elegant">elegant</option>
      <option value="erlang-dark">erlang-dark</option>
      <option value="gruvbox-dark">gruvbox-dark</option>
      <option value="lesser-dark">lesser-dark</option>
      <option value="mbo">mbo</option>
      <option value="midnight">midnight</option>
      <option value="monokai">monokai</option>
      <option value="neat">neat</option>
      <option value="neo">neo</option>
      <option value="night">night</option>
      <option value="rubyblue">rubyblue</option>
      <option value="solarized dark">solarized dark</option>
      <option value="solarized light">solarized light</option>
      <option value="tomorrow-night-bright">tomorrow-night-bright</option>
      <option value="tomorrow-night-eighties">tomorrow-night-eighties</option>
      <option value="twilight">twilight</option>
      <option value="vibrant-ink">vibrant-ink</option>
      <option value="xq-dark">xq-dark</option>
      <option value="xq-light">xq-light</option>
      <option value="yonce">yonce</option>
      <option value="zenburn">zenburn</option>`;

  function selectTheme() {
    var theme = select.options[select.selectedIndex].textContent;
    changeTheme(theme);
    setThemeOption(theme);
  }

  select.addEventListener("change", selectTheme);

  document
    .querySelector(
      "#p6n-action-bar-container-query-panel-title-bar > div.md-toolbar-tools > div.p6n-action-bar-layout-parent > div.p6n-action-bar-layout-region.p6n-action-bar-contents-right-aligned > pan-action-bar-right"
    )
    .appendChild(select);
}

function setThemeOption(theme) {
  chrome.storage.sync.set({ theme }, function () {});
}

function changeTheme(theme) {
  var cmEl = document.querySelector(".CodeMirror");
  cmEl.className = document
    .querySelector(".CodeMirror")
    .className.replace(/cm-s-[\w-]+/, `cm-s-${theme}`);
}
