const buttonId = "bigViewButton";
let fullScreen = false;

const topPlatformBar = "pcc-platform-bar";
const topActionBar = "cfc-action-bar";
const leftPanelSelector =
  "cfc-panel.cfc-panel.cfc-panel-color-grey.cfc-panel-orientation-vertical";

function toggleFullScreen(show) {
  toggleElement(show, topPlatformBar);

  toggleElement(show, topActionBar);

  toggleElement(show, leftPanelSelector);

  // toggleElement(show, "div.p6n-panel-splitter-vertical");
}

function toggleElement(show, selector) {
  document.querySelector(selector).style.display = show ? "" : "none";
}

function showButton() {
  var button = document.getElementById(buttonId);
  if (button) {
    return;
  }
  button = document.createElement("pan-action-bar-button");
  button.role = "button";
  button.id = buttonId;

  button.innerHTML = `<div
    class="p6n-action-bar-button-container"
    aria-disabled="false"
    tabindex="-1"
  >
    <button
      class="p6n-material-button p6n-action-bar-button md-primary md-button md-ink-ripple"
      type="button"
      aria-disabled="false"
    >
      <div
        class="p6n-action-bar-button-background"
        style="background-color: rgb(59, 120, 231);"
      ></div>
      <pan-icon
        icon="link"
        size="buttonCtrl.getIconSize()"
        inner-aria-label="buttonCtrl.getIconAriaLabel()"
        class="p6n-material-button-icon"
        style="fill: rgb(59, 120, 231);"
      >
        <div class="p6n-icon p6n-icon-18 p6n-icon-link">
          <md-icon class="" md-svg-icon="icon-18:link" role="img"
            ><svg
              id="full-screen_cache482"
              width="100%"
              height="100%"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
              fit=""
              preserveAspectRatio="xMidYMid meet"
              focusable="false"
            >
              <path
                d="M19,12H17V15H14V17H19V12M7,9H10V7H5V12H7V9M21,3H3A2,2 0 0,0 1,5V19A2,2 0 0,0 3,21H21A2,2 0 0,0 23,19V5A2,2 0 0,0 21,3M21,19H3V5H21V19Z"
                fill-rule="evenodd"
              ></path>
            </svg> </md-icon
          ><!---->
        </div></pan-icon
      ><!---->
      <span> BigView </span></button
    ><!---->
  </div>`;

  button.addEventListener("click", () => {
    toggleFullScreen(fullScreen);
    fullScreen = !fullScreen;
  });

  document
    .querySelector(
      "#p6n-action-bar-container-query-panel-title-bar > div.md-toolbar-tools > div.p6n-action-bar-layout-parent > div.p6n-action-bar-layout-region.p6n-action-bar-contents-right-aligned > pan-action-bar-right"
    )
    .appendChild(button);
}
