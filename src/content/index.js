// window.addEventListener("load",initBigView;

chrome.runtime.onMessage.addListener(({ runInit }, sender, sendResponse) => {
  if (runInit) {
    initBigView()
      .then(() => sendResponse({ success: true }))
      .catch((error) => {
        sendResponse({ success: false, error });
      });
    return true;
  }
});

async function initBigView() {
  const theme = await getTheme();
  let count = 0;
  let themeDone,
    buttonDone,
    selectDone = false;

  return new Promise((resolve, reject) => {
    let checkExists = setInterval(() => {
      count++;

      try {
        if (!themeDone && theme) changeTheme(theme);
        themeDone = true;

        if (!buttonDone) showButton();
        buttonDone = true;

        if (!selectDone) showSelect();
        selectDone = true;

        checkExists = clearInterval(checkExists);
        resolve();
      } catch (e) {
        if (count > 50) {
          console.error(e);

          checkExists = clearInterval(checkExists);
          reject(e);
        }
      }
    }, 200);
  });
}

function getTheme() {
  return new Promise((resolve) => {
    chrome.storage.sync.get("theme", ({ theme }) => resolve(theme));
  });
}
