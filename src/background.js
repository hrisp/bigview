const bigQueryConsolePath = "https://console.cloud.google.com/bigquery?";

chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: {
              urlContains: bigQueryConsolePath,
            },
          }),
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()],
      },
    ]);
  });
});

chrome.webNavigation.onHistoryStateUpdated.addListener((event) => {
  console.log("onHistoryStateUpdated", event);

  if (event.url.indexOf(bigQueryConsolePath) >= 0) {
    let tabId = event.tabId;
    console.log(`requesting init from tab ${tabId}`);

    chrome.tabs.sendMessage(
      tabId,
      {
        runInit: true,
      },
      (resp) =>
        resp.success
          ? console.log("init successfull")
          : console.error("init failed", resp.error)
    );
  }
});
